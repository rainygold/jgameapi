### Build & Run
* IDE: Run Main in IntelliJ
* JAR file is provided in the repository in out/artifacts/jGameAPI_jar.
* Run command in terminal 'java -jar jGameAPI.jar' (requires Java 11)
* localhost:4567/games/:id (or) localhost:4567/games/report

### Tools
* OS: Ubuntu 18.10
* Editor: IntelliJ Comm. 2019
* Java 11
* Testing: JUnit5
* Dependencies: Maven
* Packages: Spark, Gson

### Ethos

A Java project which provides JSON data via endpoints (localhost) using the Spark framework.

The data is imported from a local JSON file with fake information regarding
video games.

### Testing
Unit tests for each class using JUnit5.

### Improvements
* Further usage of Streams and Lambda operations - latter was used once.
* Integration testing
* More extensive unit testing
* More negative testing
