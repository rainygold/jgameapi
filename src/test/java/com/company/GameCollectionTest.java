package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// written with triple A pattern
class GameCollectionTest {

    @Test
    void findUserWithMostComments() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();

        // Act
        var testValue = jsonHandler.readJsonFile().findUserWithMostComments();

        // Assert
        Assertions.assertTrue(testValue.contains("test"));
    }

    @Test
    void findGameWithMostLikes() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();

        // Act
        var testValue = jsonHandler.readJsonFile().findGameWithMostLikes();

        // Assert
        Assertions.assertTrue(testValue.equals("Uncharted 4"));
    }

    @Test
    void findAverageLikesForEachGame() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();

        // Act
        var testValue = jsonHandler.readJsonFile().findAverageLikesForEachGame();

        // Assert
        Assertions.assertTrue(!testValue.isEmpty());
    }
}