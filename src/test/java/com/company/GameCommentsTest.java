package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// written with triple A pattern
class GameCommentsTest {

    @Test
    void convertDateCreated() {
        // Arrange
        String user = "Johnny Harris";
        String message = "Love this game.";
        String dateCreated = "1294012800";
        int like = 3;
        GameComments gameComments = new GameComments(user, message, dateCreated, like);

        // Act
        gameComments.convertDateCreated();

        // Assert
        Assertions.assertEquals(gameComments.dateCreated, "2011-01-03");
    }
}