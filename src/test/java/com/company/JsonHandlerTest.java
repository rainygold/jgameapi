package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// written with triple A pattern
class JsonHandlerTest {

    @Test
    void readJsonFile() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();

        // Act
        var testValue = jsonHandler.readJsonFile();

        // Assert
        Assertions.assertTrue(testValue.gameObjects.get(0).title.equals("Uncharted 4"));
    }

    @Test
    void removeIdConvertDateAndReturnGameObjectAsJson() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();
        GameCollection gameCollection = jsonHandler.readJsonFile();

        // Act
        var testValue = jsonHandler.removeIdConvertDateAndReturnGameObjectAsJson(gameCollection, 2);

        // Assert
        Assertions.assertTrue(testValue.contains("Call of Duty"));

    }

    // same as prior except with an invalid id
    @Test
    void removeIdConvertDateAndReturnGameObjectAsJsonNegative() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();
        GameCollection gameCollection = jsonHandler.readJsonFile();

        // Act
        var testValue = jsonHandler.removeIdConvertDateAndReturnGameObjectAsJson(gameCollection, 8);

        // Assert
        Assertions.assertTrue(testValue.contains("Game id not found"));

    }

    @Test
    void returnReportAsJson() {
        // Arrange
        JsonHandler jsonHandler = new JsonHandler();
        GameCollection gameCollection = jsonHandler.readJsonFile();

        // Act
        var testValue = jsonHandler.returnReportAsJson(gameCollection);

        // Assert
        Assertions.assertTrue(testValue.contains("Deadpool"));
    }
}