package com.company;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SparkHandlerTest {

    @Test
    void createEndpointsForJsonWithId() throws IOException, InterruptedException {

        // Arrange
        SparkHandler sparkHandler = new SparkHandler();
        String stringUrl = "http://localhost:4567/games/1";
        URL url = new URL(stringUrl);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        // Act
        sparkHandler.createEndpointsForJson();
        TimeUnit.SECONDS.sleep(1);
        urlConnection.connect();

        // Assert
        assertEquals(HttpURLConnection.HTTP_OK, urlConnection.getResponseCode());

    }

    @Test
    void createEndpointsForJsonReport() throws IOException, InterruptedException {

        // Arrange
        SparkHandler sparkHandler = new SparkHandler();
        String stringUrl = "http://localhost:4567/games/report";
        URL url = new URL(stringUrl);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        // Act
        sparkHandler.createEndpointsForJson();
        TimeUnit.SECONDS.sleep(1);
        urlConnection.connect();

        // Assert
        assertEquals(HttpURLConnection.HTTP_OK, urlConnection.getResponseCode());

    }
}