package com.company;

import com.google.gson.annotations.SerializedName;

public class ReportLikes {

    // class variables
    @SerializedName("title")
    protected String title;

    @SerializedName("average_likes")
    protected int averageLikes;

    // constructor
    public ReportLikes(String title, int averageLikes) {
        this.title = title;
        this.averageLikes = averageLikes;
    }
}
