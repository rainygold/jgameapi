package com.company;

import static spark.Spark.get;

public class SparkHandler {

    // class variables
    private JsonHandler jsonHandler;

    // getters and setters
    public JsonHandler getJsonHandler() {
        return jsonHandler;
    }

    public void setJsonHandler(JsonHandler jsonHandler) {
        this.jsonHandler = jsonHandler;
    }

    // constructor
    public SparkHandler() {
        this.jsonHandler = new JsonHandler();
    }

    // class methods

    // provides endpoints
    public void createEndpointsForJson() {

        // returns json report with various stats
        get("/games/report", (request, response) -> {
            response.type("application/json");

            var jsonData = this.jsonHandler.readJsonFile();
            var jsonReport = this.jsonHandler.returnReportAsJson(jsonData);
            return jsonReport;
        });

        // returns json data depending on id parameter
        get("/games/:id", (request, response) -> {
            response.type("application/json");

            var jsonData = this.jsonHandler.readJsonFile();
            var id = request.params("id");
            var jsonDataWithIdRemovedAndDatesConverted = this.jsonHandler.removeIdConvertDateAndReturnGameObjectAsJson(jsonData, Integer.parseInt(id));
            return jsonDataWithIdRemovedAndDatesConverted;
        });
    }
}
