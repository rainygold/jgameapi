package com.company;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GameCollection {

    // class variables
    @SerializedName("games")
    protected ArrayList<GameObject> gameObjects;

    // class methods

    // searches gamecollection and returns the user with the most comments
    public String findUserWithMostComments() {

        HashMap<String, Integer> counterForUsers = new HashMap<>();

        // iterate through all comments and count the user's appearances
        for (GameObject gameObject : this.gameObjects) {
            for (GameComments comments : gameObject.comments) {
                if (counterForUsers.containsKey(comments.user)) {
                    counterForUsers.replace(comments.user, counterForUsers.get(comments.user) + 1);
                } else {
                    counterForUsers.put(comments.user, 1);
                }
            }
        }

        // iterate through hashmap and find largest value
        Map.Entry<String, Integer> maxEntry = null;

        for (Map.Entry<String, Integer> entry : counterForUsers.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }

        // return entry's value
        var mostCommentedUser = maxEntry.getKey();

        return mostCommentedUser;
    }

    // searches gamecollection and returns the game with the most likes
    public String findGameWithMostLikes() {

        int highestLikes = Integer.MIN_VALUE;
        String nameOfGame = "";

        for (GameObject gameObject : this.gameObjects) {
            if (gameObject.likes > highestLikes) {
                highestLikes = gameObject.likes;
                nameOfGame = gameObject.title;
            }
        }
        return nameOfGame;
    }

    public ArrayList findAverageLikesForEachGame() {

        HashMap<String, ArrayList<Integer>> collectionOfGameTitlesAndLikes = new HashMap<>();
        ArrayList collectionOfGameTitlesAndAverages = new ArrayList<ReportLikes>();

        // iterate through all games and their comments
        for (int i = 0; i < this.gameObjects.size(); i++) {
            for (GameComments gameComments : this.gameObjects.get(i).comments) {
                if (!collectionOfGameTitlesAndLikes.containsKey(this.gameObjects.get(i).title)) {
                    collectionOfGameTitlesAndLikes.put(this.gameObjects.get(i).title, new ArrayList<>());
                }

                // add the like to the collection
                collectionOfGameTitlesAndLikes.get(this.gameObjects.get(i).title).add(gameComments.like);
            }
        }

        // sum each int in the collection and then find the average
        collectionOfGameTitlesAndLikes.forEach((key, value) -> {

            // (trying out the functional version of summing a range of ints)
            var sum = value.stream().mapToInt(integer -> integer).average().orElse(Double.NaN);
            var newReportLikes = new ReportLikes(key, (int) sum);
            collectionOfGameTitlesAndAverages.add(newReportLikes);
        });

        return collectionOfGameTitlesAndAverages;
    }
}
