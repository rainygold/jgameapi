package com.company;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonHandler {

    // class variables
    private Gson gson;

    // getters and setters
    public void setGson(Gson gson) {
        this.gson = gson;
    }

    // constructor
    public JsonHandler() {
        this.gson = new Gson();
    }

    // class methods

    // returns a GameCollection object containing all JSON data
    public GameCollection readJsonFile() {

        // finds the json in resources directory
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("games_data.json").getFile());

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // uses gson package to migrate json to GameCollection/GameObject/GameComments
        GameCollection gameObjects = gson.fromJson(br, GameCollection.class);
        return gameObjects;
    }

    // returns a specific gameobject and removes the id and converts the date
    public String removeIdConvertDateAndReturnGameObjectAsJson(GameCollection gameCollection, int id) {

        // check that the entry exists
        if (gameCollection.gameObjects.size() > id - 1) {
            // remove the id as a requirement
            gameCollection.gameObjects.get(id - 1).gameId = null;

            // convert all the epochs to conventional dates
            for (GameComments comments : gameCollection.gameObjects.get(id - 1).comments) {
                comments.convertDateCreated();
            }

            // return it as a jsonstring
            return this.gson.toJson(gameCollection.gameObjects.get(id - 1));

            // provide message for the user if their game wasn't found
        } else {
            return gson.toJson("Game id not found, please try a different number.");
        }
    }

    // returns a reportobject as a json string
    public String returnReportAsJson(GameCollection gameCollection) {
        var mostCommentedUser = gameCollection.findUserWithMostComments();
        var gameWithHighestLikes = gameCollection.findGameWithMostLikes();
        var gamesTitlesAndAverages = gameCollection.findAverageLikesForEachGame();

        // insert all the gathered data into a reportobject
        ReportObject reportObject = new ReportObject(mostCommentedUser, gameWithHighestLikes, gamesTitlesAndAverages);

        return this.gson.toJson(reportObject);
    }


}
