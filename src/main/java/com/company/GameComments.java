package com.company;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GameComments {

    // class variables
    @SerializedName("user")
    protected String user;

    @SerializedName("message")
    protected String message;

    @SerializedName("dateCreated")
    protected String dateCreated;

    @SerializedName("like")
    protected int like;

    // constructor
    public GameComments(String user, String message, String dateCreated, int like) {
        this.dateCreated = dateCreated;
        this.message = message;
        this.like = like;
        this.user = user;
    }

    // class methods

    // converts epoch time to conventional date
    protected void convertDateCreated() {
        Date date = new Date(Long.parseLong(this.dateCreated) * 1000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.dateCreated = simpleDateFormat.format(date);

    }
}
