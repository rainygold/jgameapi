package com.company;

public class Main {

    public static void main(String[] args) {

        // object setup
        SparkHandler sparkHandler = new SparkHandler();

        // endpoints setup
        sparkHandler.createEndpointsForJson();
    }
}
