package com.company;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class GameObject {

    // class variables
    @SerializedName("game_id")
    protected Integer gameId;

    @SerializedName("title")
    protected String title;

    @SerializedName("description")
    protected String description;

    @SerializedName("by")
    protected String by;

    @SerializedName("platform")
    protected String[] platform;

    @SerializedName("age_rating")
    protected String ageRating;

    @SerializedName("likes")
    protected Integer likes;

    @SerializedName("comments")
    protected ArrayList<GameComments> comments;

    // constructor
    public GameObject(Integer gameId, String title, String description, String by, String[] platform, String ageRating, Integer likes, ArrayList<GameComments> comments) {
        this.gameId = gameId;
        this.title = title;
        this.description = description;
        this.by = by;
        this.platform = platform;
        this.ageRating = ageRating;
        this.likes = likes;
        this.comments = comments;
    }
}
