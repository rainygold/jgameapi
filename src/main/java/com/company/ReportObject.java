package com.company;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportObject {

    // class variables
    @SerializedName("user_with_most_comments")
    protected String userWithMostComments;

    @SerializedName("highest_rated_game")
    protected String highestRatedGame;

    @SerializedName("average_likes_per_game")
    protected ArrayList<ReportLikes> averageLikesPerGame;

    // constructor
    public ReportObject(String userWithMostComments, String highestRatedGame, ArrayList<ReportLikes> averageLikesPerGame) {
        this.userWithMostComments = userWithMostComments;
        this.highestRatedGame = highestRatedGame;
        this.averageLikesPerGame = averageLikesPerGame;
    }
}
